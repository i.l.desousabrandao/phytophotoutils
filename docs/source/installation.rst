============
Installation
============

Download and install with the latest updates:

   | $ git clone git@gitlab.com:tjryankeogh/phytophotoutils.git  
   | $ cd PhytoPhotoUtils  
   | $ python setup.py install  