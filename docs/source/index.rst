.. PhytoPhotoUtils documentation master file, created by
   sphinx-quickstart on Wed Apr  7 23:22:53 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PhytoPhotoUtils's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Getting started

   installation

.. toctree::
   :maxdepth: 2
   :caption: Examples and SOP

   loading
   saturation
   relaxation
   tools
   etr
   spectral
   plot


.. toctree::
   :maxdepth: 2
   :caption: Help and Reference

   api
   citing
   history
   contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
